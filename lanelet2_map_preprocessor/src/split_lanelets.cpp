/*
 * Copyright 2020 Tier IV, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <ros/ros.h>

#include <lanelet2_core/LaneletMap.h>
#include <lanelet2_core/geometry/Lanelet.h>
#include <lanelet2_io/Io.h>

#include <lanelet2_extension/io/autoware_osm_parser.h>
#include <lanelet2_extension/projection/mgrs_projector.h>
#include <lanelet2_extension/utility/message_conversion.h>

#include <iostream>
#include <unordered_set>
#include <vector>

void printUsage()
{
  std::cerr << "Please set following private parameters:" << std::endl
            << "llt_map_path" << std::endl
            << "output_path" << std::endl
            << "lanelet_length" << std::endl
            ;
}

bool loadLaneletMap(
  const std::string & llt_map_path, lanelet::LaneletMapPtr & lanelet_map_ptr,
  lanelet::Projector & projector)
{
  lanelet::LaneletMapPtr lanelet_map;
  lanelet::ErrorMessages errors;
  lanelet_map_ptr = lanelet::load(llt_map_path, "autoware_osm_handler", projector, &errors);

  for (const auto & error : errors) {
    ROS_ERROR_STREAM(error);
  }
  if (!errors.empty()) {
    return false;
  }
  std::cout << "Loaded Lanelet2 map" << std::endl;
  return true;
}

bool exists(std::unordered_set<lanelet::Id> & set, lanelet::Id element)
{
  return std::find(set.begin(), set.end(), element) != set.end();
}

lanelet::Points3d convertPointsLayerToPoints(lanelet::LaneletMapPtr & lanelet_map_ptr)
{
  lanelet::Points3d points;
  for (const lanelet::Point3d pt : lanelet_map_ptr->pointLayer) {
    points.push_back(pt);
  }
  return points;
}

lanelet::LineStrings3d convertLineLayerToLineStrings(lanelet::LaneletMapPtr & lanelet_map_ptr)
{
  lanelet::LineStrings3d lines;
  for (const lanelet::LineString3d line : lanelet_map_ptr->lineStringLayer) {
    lines.push_back(line);
  }
  return lines;
}


std::vector<lanelet::LineString3d> splitline(
	const lanelet::ConstLineString3d & line, 
	lanelet::LineString3d, const double lanelet_length
)
{
	std::vector<lanelet::LineString3d> newlines;
	
	lanelet::LineString3d newline;
	std::double accumulated_distance = 0;
	
	std::vector<lanelet::BasicPoint3d> points;
		
	for (size_t i = 0; i < line.size(); i++) {
		if (i == 0){
			points.push_back(line[i]);
			continue;
		}

    	lanelet::BasicPoint3d back_point = line[i-1];
    	lanelet::BasicPoint3d front_point = line[i];
		const auto direction_vector = (front_point - back_point);
				
		lanelet::BasicPoint3d check_point = front_point;
		double distance = boost::geometry::distance(back_point, front_point);
				

		accumulated_distance += distance;
		while (accumulated_distance >= lanelet_length){
			const auto target_point = back_point + (direction_vector * lanelet_length);
			points.push_back(target_point);
			
			lanelet::LineString3d newline(points, line.attributes());
			newlines.push_back(newline);
			
			points.empty();
			points.push_back(target_point);
			
			back_point = target_point;
			accumulated_distance -= lanelet_length;
		}
			
		points.push_back(front_point);
	}
	
	lanelet::LineString3d newline(points, line.attributes());
	newlines.push_back(newline);	
	
	return newlines;
}


void splitLanelets(lanelet::LaneletMapPtr lanelet_map, const double lanelet_length)
{

	auto lanelets = utils::transform(lanelet_map->laneletLayer, [](const ConstLanelet& llt) {
		return Lanelet(std::const_pointer_cast<LaneletData>(llt.constData()), llt.inverted());
	});

	for (auto & lanelet_obj : lanelets) {
		const double left_length = lanelet::geometry::length(lanelet_obj.leftBound());
		const double right_length = lanelet::geometry::length(lanelet_obj.rightBound());
		const double length_coef = left_length / right_length;

	    const auto leftline = lanelet_obj.leftBound();
	    const auto rightline = lanelet_obj.rightBound();

		if (left_length < lanelet_length || right_length < lanelet_length){
			continue;
		}

	  
		auto leftlines = splitline(leftline, lanelet_length * (length_coef >= 1 ? length_coef : (1 / length_coef));
		auto rightlines = splitline(rightline, lanelet_length * (length_coef <= 1 ? length_coef : (1 / length_coef));
		
		if (leftlines.size() != rightlines.size() || leftlines.size() == 1){
			continue;
		}
		
		for (int i = 0; i < leftlines.size(); i++) {

			// Заменяем линию в текущем lanelet
			if (i == 0){
				lanelet_obj.setLeftBound(leftlines.at(i));
				lanelet_obj.setRightBound(rightlines.at(i));
			} else {
				lanelet::Lanelet lanelet(lanelet::utils::getId(), leftlines.at(i), rightlines.at(i), lanelet_obj.attributes());
				lanelets.push_back(lanelet);
			}
		}
	}
	
	  lanelet::LaneletMapPtr new_map(new lanelet::LaneletMap);
	  for (auto llt : lanelet_map_ptr->laneletLayer) {
		new_map->add(llt);
	  }
	  lanelet_map_ptr = new_map;
}



void removeUnreferencedGeometry(lanelet::LaneletMapPtr & lanelet_map_ptr)
{
  lanelet::LaneletMapPtr new_map(new lanelet::LaneletMap);
  for (auto llt : lanelet_map_ptr->laneletLayer) {
    new_map->add(llt);
  }
  lanelet_map_ptr = new_map;
}

int main(int argc, char * argv[])
{
  ros::init(argc, argv, "split_lanelets");
  ros::NodeHandle pnh("~");

  if (!pnh.hasParam("llt_map_path")) {
    printUsage();
    return EXIT_FAILURE;
  }
  if (!pnh.hasParam("output_path")) {
    printUsage();
    return EXIT_FAILURE;
  }
  
  if (!pnh.hasParam("lanelet_length")) {
    printUsage();
    return EXIT_FAILURE;
  }  

  std::string llt_map_path, output_path;
  double lanelet_length;
  pnh.getParam("llt_map_path", llt_map_path);
  pnh.getParam("output_path", output_path);
  pnh.getParam("lanelet_length", lanelet_length);
  
  lanelet::LaneletMapPtr llt_map_ptr(new lanelet::LaneletMap);
  lanelet::projection::MGRSProjector projector;

  if (!loadLaneletMap(llt_map_path, llt_map_ptr, projector)) {
    return EXIT_FAILURE;
  }
  splitLanelets(llt_map_ptr);
  lanelet::write(output_path, *llt_map_ptr, projector);

  return 0;
}
